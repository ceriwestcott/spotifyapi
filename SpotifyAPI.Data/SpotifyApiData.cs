﻿using System;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using SpotifyAPI.Data.Configuration;
using SpotifyAPI.Models;

namespace SpotifyAPI.Data
{
    public class SpotifyApiData
    {
        private string _connectionString;
        private string _AllArtists = "SELECT * FROM UnfoundArtists";
        private string _AllFestivals = "SELECT * FROM FESTIVALS";
        private string _ALLPlaylists = "SELECT * FROM Playlists";
        private string _ClientData = "SELECT ClientData, SecretData FROM ClientData";
        public string ConnectionString { get => _connectionString; set => _connectionString = value; }
        

        public SpotifyApiData()
        {
            _connectionString = ConfigurationHelper.GetConnectionString();
        }


        public CompleteData GetAllData()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                var artist = connection.Query<UnfoundArtists>(_AllArtists);
                var playlists = connection.Query<Playlists>(_ALLPlaylists);
                var festivals = connection.Query<Festivals>(_AllFestivals);
                var clientData = connection.Query<ClientData>(_ClientData);
                return new CompleteData
                {
                    Festivals = festivals.ToList(),
                    Playlist = playlists.ToList(),
                    UnfoundArtists = artist.ToList(),
                    ClientData = clientData.ToList()
                };
            }
        }
    }
}
