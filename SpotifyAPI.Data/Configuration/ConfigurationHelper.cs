﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace SpotifyAPI.Data.Configuration
{
    public class ConfigurationHelper
    {
        private string _connectionString;
        public string ConnectionString { get => _connectionString; set => _connectionString = value; }
        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

       
    }
}
