﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using HtmlAgilityPack;

namespace SpotifyApiConsole.Helpers
{
    public class Webscraper
    {
        public static string[] artists = { };

        public static string[] GetDownloadArtists()
        {
            var result = new WebClient().DownloadString("https://downloadfestival.co.uk/artists-a-z/");
            var doc = new HtmlDocument();
            doc.LoadHtml(result);
            var pre = doc.DocumentNode.Descendants("nav").Where(d => d.Attributes["class"].Value.Contains("artist"));
            return pre.First().InnerText.Split(new[] { "\n" }, StringSplitOptions.None).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
        }
    }
}
