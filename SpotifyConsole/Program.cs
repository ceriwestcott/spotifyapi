﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using HtmlAgilityPack;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Auth;
using SpotifyAPI.Web.Enums;
using SpotifyAPI.Web.Models;
using Token = SpotifyAPI.Web.Models.Token;

namespace SpotifyApi
{
    class Program
    {
        private static string _clientId = "eca82f597115423cac9d1125e0fb97c4"; //"";
        private static string _secretId = "17a6e5916bb3424eb50f29e4816521a4";

        public static string[] downloadArtists = { };


        static void Main(string[] args)
        {
            _clientId = string.IsNullOrEmpty(_clientId)
                ? Environment.GetEnvironmentVariable("SPOTIFY_CLIENT_ID")
                : _clientId;

            _secretId = string.IsNullOrEmpty(_secretId)
                ? Environment.GetEnvironmentVariable("SPOTIFY_SECRET_ID")
                : _secretId;

            Console.WriteLine("####### Spotify API Playlist Generator #######");
            Console.WriteLine(
                "Tip: If you want to supply your ClientID and SecretId beforehand, use env variables (SPOTIFY_CLIENT_ID and SPOTIFY_SECRET_ID)");


            AuthorizationCodeAuth auth =
                new AuthorizationCodeAuth(_clientId, _secretId, "http://localhost:4003", "http://localhost:4003",
                    Scope.PlaylistModifyPrivate | Scope.PlaylistReadCollaborative);

            auth.AuthReceived += AuthOnAuthReceived;
            auth.Start();
            auth.OpenBrowser();

            Console.ReadLine();
            auth.Stop(0);
        }

        private static async void AuthOnAuthReceived(object sender, AuthorizationCode payload)
        {
            AuthorizationCodeAuth auth = (AuthorizationCodeAuth)sender;
            auth.Stop();

            Token token = await auth.ExchangeCode(payload.Code);
            SpotifyWebAPI api = new SpotifyWebAPI
            {
                AccessToken = token.AccessToken,
                TokenType = token.TokenType
            };
            GetArtists();
            GeneratePlaylist(api);
        }

        private static void GetArtists()
        {
            var result = new WebClient().DownloadString("https://downloadfestival.co.uk/artists-a-z/");
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(result);
            var pre = doc.DocumentNode.Descendants("nav").Where(d => d.Attributes["class"].Value.Contains("artist"));
            downloadArtists = pre.First().InnerText.Split(new[] { "\n" }, StringSplitOptions.None).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
        }

        private static void GeneratePlaylist(SpotifyWebAPI api)
        {
            List<string> songIdList = new List<string>();
            List<string> notFound = new List<string>();
            string playlistId = "4l7BpS8kIU6xcmVkijHl5g";

            var myUserId = api.GetPublicProfile("1110658906");
            var playlist = api.GetPlaylist(myUserId.Id, playlistId);
            List<Tuple<string, string>> edgeCases = GetEdgeCases();
            foreach (var item in downloadArtists)
            {
                SearchItem searchItem = null;
                foreach (Tuple<string, string> artist in edgeCases)
                {
                    if (item.Contains(artist.Item1))
                    {
                        searchItem = api.SearchItems(artist.Item2, SearchType.Artist, limit: 1);
                    }
                    else
                    {
                        searchItem = api.SearchItems(WebUtility.HtmlDecode(item), SearchType.Artist, limit: 1);

                    }

                }

                if (searchItem.Artists != null)
                {
                    if (searchItem.Artists.Items.Any())
                    {
                        SeveralTracks topTracks = api.GetArtistsTopTracks(searchItem.Artists.Items.First().Id, "GB");
                        if (topTracks.Tracks != null)
                        {
                            Console.WriteLine(searchItem.Artists.Items.First().Name + " " +
                                              searchItem.Artists.Items.First().Id);
                            for (int i = 0; i < 3; i++)
                            {
                                songIdList.Add(topTracks.Tracks[i].Uri);
                                Console.WriteLine(topTracks.Tracks[i].Name);
                            }

                            Console.WriteLine("\n\n");
                        }
                        else
                        {
                            notFound.Add(WebUtility.HtmlDecode(item));
                        }
                    }
                    else
                    {
                        notFound.Add(WebUtility.HtmlDecode(item));
                    }
                }
            }


            ErrorResponse respones = api.AddPlaylistTracks(myUserId.Id, playlist.Id, songIdList);


            foreach (var track in notFound)
                Console.WriteLine("Not found: " + track);
            Console.WriteLine("Playlist Created");
        }

        public static List<Tuple<string, string>> GetEdgeCases()
        {
            List<Tuple<string, string>> artists = new List<Tuple<string, string>>();
            artists.Add(new Tuple<string, string>("Slash", "Slash"));
            return artists;
        }
    }
}
