﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotifyAPI.Models
{
    public class ClientData
    {
        public string ClientID { get; set; }
        public string SecretID { get; set; }
    }
}
