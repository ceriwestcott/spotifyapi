﻿using System.Linq;
using SpotifyAPI.Data.Configuration;
using SpotifyAPI.Models;
using SpotifyAPI.Service;

namespace SpotifyAPI.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            SpotifyBO spotifyBO = new SpotifyBO();

            spotifyBO.GetAllData();
            spotifyBO.AuthorizeAndScrape();


            System.Console.ReadLine();
        }
    }
}
