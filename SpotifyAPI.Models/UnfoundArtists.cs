﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SpotifyAPI.Models
{
    [Table("UnfoundArtists")]
    public class UnfoundArtists
    {
        public int UnfoundArtistID { get; set; }
        public int FestivalID { get; set; }
        public string ArtistName { get; set; }
        public string ArtistSearchTerm { get; set; }
        public bool ToSearch { get; set; }
    }
}
