﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SpotifyAPI.Models
{
    [Table("Festivals")]
    public class Festivals
    {
        public int FestivalID { get; set; }
        public string FestivalName { get; set; }
        public DateTime Year { get; set; }
    }
}
