﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using SpotifyAPI.Data;
using SpotifyAPI.Models;
using SpotifyAPI.Service.Factory;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Auth;
using SpotifyAPI.Web.Enums;
using SpotifyAPI.Web.Models;

namespace SpotifyAPI.Service
{
    public class SpotifyBO
    {
        private readonly SpotifyApiData _spotifyData;
        private static CompleteData _completeData;
        private static SpotifyWebAPI _api;
        private static List<string> _songIDList;
        private static PublicProfile _myPublicProfile;

        public SpotifyBO()
        {
            _spotifyData = new SpotifyApiData();
            _songIDList = new List<string>();
        }

        public void GetAllData()
        {
            _completeData = _spotifyData.GetAllData();
        }
        //Authorization to allow me to use the spotifyAPI
        public void AuthorizeAndScrape()
        {
            string _clientID = _completeData.ClientData.First().ClientID;
            string _secretID = _completeData.ClientData.First().SecretID;
            AuthorizationCodeAuth auth =
                new AuthorizationCodeAuth(_clientID, _secretID, "http://localhost:4003", "http://localhost:4003",
                    Scope.PlaylistModifyPrivate | Scope.PlaylistReadCollaborative);

            //Once auth has been received, call AuthOnAuthReceieved method
            auth.AuthReceived +=  AuthOnAuthReceived;
            auth.Start();
            auth.OpenBrowser();
        }

        private static async void AuthOnAuthReceived(object sender, AuthorizationCode payload)
        {
            AuthorizationCodeAuth auth = (AuthorizationCodeAuth) sender;
            auth.Stop();

            Token token = await auth.ExchangeCode(payload.Code);
            _api = new SpotifyWebAPI
            {
                AccessToken = token.AccessToken,
                TokenType = token.TokenType
            };

            
            ScrapeAndGenerate();
        }

        public static void ScrapeAndGenerate()
        {
            _myPublicProfile = _api.GetPublicProfile("1110658906");
            foreach (Festivals festival in _completeData.Festivals)
            {
                string[] artistNames = FestivalFactory.ScrapeWeb(festival.FestivalID);
                Playlists playlist = _completeData.Playlist.First(x => x.FestivalID == festival.FestivalID);
                List<UnfoundArtists> unknownArtists =
                    _completeData.UnfoundArtists.FindAll(x => x.FestivalID == festival.FestivalID);
                //Loop through all artists that have been scraped from the web page
                foreach (string artist in artistNames)
                {
                    bool skipArtist = false;
                    SearchItem searchItem = null;
                    //Check if any of those artists exist in the UnfoundArtists database - more or less check if the name is different on spotify or if they are even on spotify
                    foreach (UnfoundArtists unfoundArtist in unknownArtists)
                    {
                        //If the download artist page contains the a specific name e.g if 'slash & the conspirators contains slash
                        if (artist.Trim().Contains(unfoundArtist.ArtistName)) { 
                            //if so, check if the artist is on spotify
                            if (unfoundArtist.ToSearch)
                            {
                                //search the artist using the spotft serach term
                                searchItem = _api.SearchItems(unfoundArtist.ArtistSearchTerm, SearchType.Artist,
                                    limit: 1);
                                break;
                            }
                            //this case is if the artist is on the page but not on spotify
                            skipArtist = true;
                        }
                        else
                        {
                            //search the artist if there is no edge case - different spotify name/on spotify at all etc
                            searchItem = _api.SearchItems(artist.Trim(), SearchType.Artist, limit: 1);
                        }

                    }

                    //checks whether the artist should be searched && search item isn't null and && seach item has an artist
                    if (skipArtist == false && searchItem != null && searchItem.Artists.Items.Any())
                    {
                        //Get top tracks
                        SeveralTracks topTracks = _api.GetArtistsTopTracks(searchItem.Artists.Items.First().Id, "GB");

                        GetRandomTopSongs(topTracks);
                    }
                    else
                    {
                        Console.WriteLine("NOT FOUND OR SKIPPED - " + WebUtility.HtmlDecode(artist));
                    }
                }
                //
                RemovePlaylistTrack(playlist);
                AddSongsPaged(playlist, _songIDList);
                
            }

        }


        #region Add/Remove songs

        //Add songs   paged due to 100 song limit at a time
        private static void AddSongsPaged(Playlists playlist, List<string> _songList)
        {
            int pageSize = 100;
            double offSet = Math.Ceiling(_songList.Count / 100.0);
            for (int i = 0; i < offSet; i++)
            {
                _api.AddPlaylistTracks(_myPublicProfile.Id, playlist.SpotifyPlaylistID.ToString(),
                    GetPaged(_songList, i, pageSize));
            }
        }

        //Remove all songs from current playlist
        private static void RemovePlaylistTrack(Playlists playlist)
        {
            FullPlaylist currentPlaylist = _api.GetPlaylist(_myPublicProfile.Id, playlist.SpotifyPlaylistID);

            int rowCount = currentPlaylist.Tracks.Total;
            List<DeleteTrackUri> _deleteTrack = new List<DeleteTrackUri>();
            //Cheap paging but it works and is sound
            while (rowCount > 0)
            {
                foreach (PlaylistTrack track in currentPlaylist.Tracks.Items)
                {
                    _deleteTrack.Add(new DeleteTrackUri(track.Track.Uri));
                }

                _api.RemovePlaylistTracks(_myPublicProfile.Id, playlist.SpotifyPlaylistID, _deleteTrack);

                currentPlaylist = _api.GetPlaylist(_myPublicProfile.Id, playlist.SpotifyPlaylistID);
                _deleteTrack.Clear();

                rowCount = currentPlaylist.Tracks.Items.Count;
            }
        }

        #endregion


        #region Helper Methods

        private static List<string> GetPaged(List<string> list, int page, int pageSize)
        {   
            //Page the search results due to 100 items at a time limit
            return list.Skip(page * pageSize).Take(pageSize).ToList();
        }

        private static void GetRandomTopSongs(SeveralTracks topTracks)
        {
            //Get 3 random top songs
            //a hashset never allows duplicates so the while loop will deal with always getting 3 random songs with no duplicates
            HashSet<int> track = new HashSet<int>();
            Random rand = new Random();
            while (track.Count < 3)
                track.Add(rand.Next(0, topTracks.Tracks.Count));
            //TODO swap to foreach
            for (int i = 0; i < track.Count; i++)
            {
                _songIDList.Add(topTracks.Tracks[track.ElementAt(i)].Uri);
            }
        }
        #endregion

    }
}
