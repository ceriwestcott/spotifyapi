﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotifyAPI.Models
{
    public class CompleteData
    {
        public List<Festivals> Festivals { get; set; }
        public List<Playlists> Playlist { get; set; }
        public List<UnfoundArtists> UnfoundArtists { get; set; }
        public List<ClientData> ClientData { get; set; }
    }
}
