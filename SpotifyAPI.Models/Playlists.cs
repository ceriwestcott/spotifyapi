﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SpotifyAPI.Models
{
    [Table("Playlists")]
    public class Playlists
    {
        [Column("PlaylistID")]
        public int PlaylistID { get; set; }
        [Column("FestivalID")]
        public int FestivalID { get; set; }
        [Column("Year")]
        public DateTime Year { get; set; }
        [Column("SpotifyPlaylistID")]
        public string SpotifyPlaylistID { get; set; }

    }
}
